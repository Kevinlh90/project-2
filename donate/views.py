from django.contrib.auth import logout as logout_user
from django.http import HttpResponseRedirect, JsonResponse, HttpResponseNotFound
from django.shortcuts import render, redirect
from project_item.models import Project, Patreon

import hashlib, uuid

# Create your views here.
def index(request):
	return render(request, 'donate.html')

def redirect_project(request, project_id):
    return HttpResponseRedirect('/project/'+project_id)

def generate_donation_page(request, pk, slug):
    title = slug.replace("-", " ")

    try:
        project = Project.objects.get(pk=pk, title=title)
        
    except Project.DoesNotExist:
        return HttpResponseNotFound("page not found")
    
    return render(request, 'donate.html', {'project':project})

def validate_donate(request):
    nominal = request.GET.get('nominal', None)
    password = request.GET.get('password', None)
    checkbox = (request.GET.get('checkbox', None) == "true")
    project_id = request.GET.get('project_id', None)
    
    print(project_id)
    try:
        v_nom = int(nominal) > 0
    except ValueError:
        v_nom = False   
    v_auth = request.user.is_authenticated
    v_password = request.user.check_password(password) #not working damnit
    v_all = v_auth & v_nom #& v_password
    
    if(v_all):           
        project = Project.objects.get(id=project_id)
        if(checkbox):
            fullname = request.user.first_name + " " + request.user.last_name
            if(not fullname or fullname.isspace()):
                fullname = request.user.username
            patreon = Patreon.objects.create(name=fullname, donation=nominal)
            project.patreon.add(patreon)
        else:
            project.anonymous_donation += int(nominal)
        project.save()
            
    data = {
        'is_submitable': v_all,
        'project_id': project_id,
        'project_slug': project.slug_title()
    }
    
    return JsonResponse(data)