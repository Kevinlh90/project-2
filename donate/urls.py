from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from .views import *

urlpatterns = [    
    path(r'donate/', index, name = 'donate'),
    url(r'^hub/project/(?P<pk>\d+)/(?P<slug>[-\w]+)/donate/$', generate_donation_page, name='generate_donation_page'),
    url(r'^redirect_project/$', redirect_project, name='redirect_project'),
    url(r'^ajax/donate/$', validate_donate, name='validate_donate'),
]

urlpatterns += staticfiles_urlpatterns()