from django.shortcuts import render
from news_article.models import Article
from project_item.models import Project
from django.http import JsonResponse

# Create your views here.
def index(request):
    
    all_article = Article.objects.order_by('id').all()
    
    top_article_first = all_article.first()
    all_article = all_article[1:]
    
    return render(request, 'hub.html', {'first_article':top_article_first,
                                        'all_article':all_article,
                                        'range':range(1,all_article.count()+1),
                                        'nbar': 'hub'})

def get_projects(request):
    category = request.GET.get('category', None)
    response = []
    
    if(category == "All"):
        projects = Project.objects.all()
    else:
        projects = Project.objects.filter(category=category)
        
    projects = projects.order_by('title')
        
    for project in projects:
        print(" :::: "+project.slug_title())
        response.append({
            'title' : project.title,
            'slug' : project.slug_title(),
            'summary' : project.summary,
            'id' : project.id,
            'progress' : project.get_progress()
        })
    
    return JsonResponse(response, safe=False)