from django.db import models
from django.contrib.staticfiles.templatetags.staticfiles import static
from project_item.models import Project
import os, uuid

# Create your models here.
def get_image_path(instance, filename):
    return os.path.join('article', filename)

class Article(models.Model):
    title = models.CharField(max_length = 250)
    category = models.CharField(max_length = 500)   
    article_image = models.ImageField(upload_to=get_image_path, blank=True, null=True)
    content = models.TextField(default="<p></p>")
    modified_date = models.DateTimeField(auto_now=True)
    projects = models.ManyToManyField(Project, blank=True)
    
    def slug_title(self):
        return self.title.replace(" ","-")
    
    @property
    def get_image(self):
        if(not self.article_image):
            return static("img/No_Image_Available.jpg")
        return self.article_image.url
    
    def __str__(self):
        return self.title