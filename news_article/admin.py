from django.contrib import admin
from .models import Article

# Register your models here.
class ArticleModelAdmin(admin.ModelAdmin):
    list_display = ["title", "category", "modified_date", "id"]
    list_filter = ["category", "modified_date"]
    
    search_fields = ["title", "category", "content"]
    
    class Meta:
        model = Article
    
admin.site.register(Article, ArticleModelAdmin)    