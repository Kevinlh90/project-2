from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from .views import *

urlpatterns = [    
    path(r'article_page/', index, name = 'article'),
    url(r'^hub/article/(?P<pk>\d+)/(?P<slug>[-\w]+)/$', generate_article_page, name='generate_article_page'),
]

urlpatterns += staticfiles_urlpatterns()