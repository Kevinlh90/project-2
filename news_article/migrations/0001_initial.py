# Generated by Django 2.1.2 on 2018-11-30 13:44

from django.db import migrations, models
import news_article.models
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('project_item', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Article',
            fields=[
                ('title', models.CharField(max_length=250)),
                ('category', models.CharField(max_length=500)),
                ('article_image', models.ImageField(blank=True, null=True, upload_to=news_article.models.get_image_path)),
                ('content', models.TextField(default='<p></p>')),
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False, unique=True)),
                ('projects', models.ManyToManyField(blank=True, to='project_item.Project')),
            ],
        ),
    ]
