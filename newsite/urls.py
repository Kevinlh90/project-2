"""newsite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from base.views import index
from project_hub.views import get_projects

urlpatterns = [
    path('admin/', admin.site.urls),
    path(r'index/', index, name = 'index'),
    path('', include(('base.urls','base'),namespace='base')),
    path('', include(('about.urls','about'),namespace='about')),
    path('', include(('login.urls','login'),namespace='login')),
    path('', include(('donate.urls','donate'),namespace='donate')),
    path('', include(('project_hub.urls','project_hub'),namespace='project_hub')),
    path('', include(('project_item.urls','project_item'),namespace='project_item')),
    path('', include(('news_article.urls','news_article'),namespace='news_article')),
    url(r'^hub/', include(('project_hub.urls', 'project_hub'), namespace='project_hub')),
    url(r'projects/$', get_projects, name='get_projects'),
    url(r'^auth/', include('social_django.urls', namespace='social')),  # <- Here
]

urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)