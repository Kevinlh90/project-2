from django.db import models
from django.contrib.staticfiles.templatetags.staticfiles import static
from django.utils.text import slugify
import os

# Create your models here.
def get_image_path(instance, filename):
    return os.path.join('article', filename)

class Patreon(models.Model):
    name = models.CharField(max_length = 300)
    donation = models.IntegerField(default = 1)
    
    def __str__(self):
        return self.name+" :: "+str(self.donation)
    
class Project(models.Model):
    title = models.CharField(max_length = 250)
    category = models.CharField(max_length = 500)
    summary = models.CharField(max_length = 500)
    content = models.TextField(default="<p></p>")
    image = models.ImageField(upload_to=get_image_path, blank=True, null=True)
    modified_date = models.DateTimeField(auto_now=True)
    
    anonymous_donation = models.IntegerField(default=0)
    max_donation = models.IntegerField(default=100)
    
    patreon = models.ManyToManyField(Patreon, blank=True)
    
    def slug_title(self):
        return self.title.replace(" ","-")
    
    def get_progress(self):
        return self.current_donation() / self.max_donation * 100
    
    @property
    def get_image(self):
        if(not self.image):
            return static("img/No_Image_Available.jpg")
        return self.image.url
    
    def current_donation(self):
        total = self.anonymous_donation
        for patreon in self.patreon.all():
            total += patreon.donation
            
        return total
    
    def __str__(self):
        return self.title+" <> "+self.category