from django.shortcuts import render
from news_article.models import Project
from django.http import HttpResponseNotFound

# Create your views here.
def index(request):
	return render(request, 'project.html')

def generate_project_page(request, pk, slug):
    title = slug.replace("-", " ")    
    try:
        project = Project.objects.get(pk=pk, title=title)
        
    except Project.DoesNotExist:
        return HttpResponseNotFound("page not found")
    
    return render(request, 'project.html', {'project':project})