from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from .views import *

urlpatterns = [    
    path(r'projecthub/', index, name = 'project'),
    url(r'^hub/project/(?P<pk>\d+)/(?P<slug>[-\w]+)/$', generate_project_page, name='generate_project_page'),
]

urlpatterns += staticfiles_urlpatterns()