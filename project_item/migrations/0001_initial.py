# Generated by Django 2.1.2 on 2018-11-30 13:44

from django.db import migrations, models
import project_item.models
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Patreon',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=300)),
                ('donation', models.IntegerField(default=1)),
            ],
        ),
        migrations.CreateModel(
            name='Project',
            fields=[
                ('title', models.CharField(max_length=250)),
                ('category', models.CharField(max_length=500)),
                ('summary', models.CharField(max_length=500)),
                ('content', models.TextField(default='<p></p>')),
                ('image', models.ImageField(blank=True, null=True, upload_to=project_item.models.get_image_path)),
                ('current_donation', models.IntegerField(default=0)),
                ('max_donation', models.IntegerField(default=100)),
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False, unique=True)),
                ('patreon', models.ManyToManyField(blank=True, to='project_item.Patreon')),
            ],
        ),
    ]
