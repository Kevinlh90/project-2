from django.contrib.auth import logout as logout_user
from django.http import HttpResponseRedirect
from django.shortcuts import render
from news_article.models import Project

# Create your views here.
def index(request):
	return render(request, 'login.html')

def logout(request):
    logout_user(request)
    return HttpResponseRedirect('/login/')